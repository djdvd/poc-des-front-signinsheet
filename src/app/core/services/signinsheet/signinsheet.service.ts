import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Signinsheet } from './../../models/signinsheet/signinsheet.model';

@Injectable({
  providedIn: 'root'
})
export class SigninsheetService {

  private baseUrl = 'http://localhost:8080/signinsheet';

  constructor(private http: HttpClient) { }

  getSigninsheet(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createSigninsheet(signinsheet: Signinsheet): Observable<any> {
    return this.http.post(`${this.baseUrl}`, signinsheet);
  }

  updateSigninsheet(signinsheet: Signinsheet): Observable<any> {
    return this.http.put(`${this.baseUrl}`, signinsheet);
  }

  deleteSigninsheet(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }
}
