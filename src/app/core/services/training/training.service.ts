import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Training } from './../../models/training/training.model';

@Injectable({
  providedIn: 'root'
})
export class TrainingService {


  private baseUrl = 'http://localhost:8080/training';

  constructor(private http: HttpClient) { }

  getTraining(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createTraining(training: Training): Observable<any> {
    return this.http.post(`${this.baseUrl}`, training);
  }

  updateTraining(training: Training): Observable<any> {
    return this.http.put(`${this.baseUrl}`, training);
  }

  deleteTraining(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }
}
