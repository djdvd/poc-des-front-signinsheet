import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Entreprise } from './../../models/entreprise/entreprise.model';

@Injectable({
  providedIn: 'root'
})
export class EntrepriseService {

  private baseUrl = 'http://localhost:8080/entreprise';

  constructor(private http: HttpClient) { }

  getEntreprise(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createEntreprise(entreprise: Entreprise): Observable<any> {
    return this.http.post(`${this.baseUrl}`, entreprise);
  }

  updateEntreprise(entreprise: Entreprise): Observable<any> {
    return this.http.put(`${this.baseUrl}`, entreprise);
  }

  deleteEntreprise(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }
}
