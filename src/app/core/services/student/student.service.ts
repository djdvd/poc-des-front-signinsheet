import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Student } from './../../models/student/student.model';


// Adding headers
const headers = new HttpHeaders()
.append('Content-Type', 'application/json')
.append('Access-Control-Allow-Origin', '*');

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private baseUrl = 'http://localhost:8080/student';

  constructor(private http: HttpClient) { }

  getStudent(id: number): Observable<any> {

     return this.http.get(`${this.baseUrl}/${id}`, {headers});
  }

  createStudent(student: Student): Observable<any> {
    return this.http.post(`${this.baseUrl}`, student, {headers});
  }

  updateStudent(student: Student): Observable<any> {
    return this.http.put(`${this.baseUrl}`, student);
  }

  deleteStudent(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }


}
