import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Teacher } from './../../models/teacher/teacher.model';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  private baseUrl = 'http://localhost:8080/teacher';

  constructor(private http: HttpClient) { }

  getTeacher(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createTeacher(teacher: Teacher): Observable<any> {
    return this.http.post(`${this.baseUrl}`, teacher);
  }

  updateTeacher(teacher: Teacher): Observable<any> {
    return this.http.put(`${this.baseUrl}`, teacher);
  }

  deleteTeacher(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }
}
