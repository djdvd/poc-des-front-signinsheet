export class Contact {
  'telephone': string;
  'email': string;
  'url': string;
}
