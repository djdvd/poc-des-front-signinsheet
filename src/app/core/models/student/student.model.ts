import { Adresse } from '../share/adresse.model';
import { Contact } from '../share/contact.model';
import { Signinsheet } from '../signinsheet/signinsheet.model';
import { Training } from '../training/training.model';

export class Student {
  id: number;
  name: string;
  category: string;
  contact: Contact;
  firstName: string;
  lastName: string;
  entreprise: string;
  trainings: Array<Training> = [];
  adresse: Adresse;
  signInSheets: Array<Signinsheet> = [];
}
