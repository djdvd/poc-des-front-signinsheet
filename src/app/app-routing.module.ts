import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { EntrepriseComponent } from './module/entreprise/components/entreprise/entreprise.component';
import { SigninsheetComponent } from './module/signinsheet/components/signinsheet/signinsheet.component';
import { StudentComponent } from './module/student/components/student/student.component';
import { TeacherComponent } from './module/teacher/components/teacher/teacher.component';

const routes: Routes = [
  { path: '', redirectTo: 'teacher', pathMatch: 'full' },
  { path: 'student', component: StudentComponent },
  { path: 'teacher', component: TeacherComponent },
  { path: 'signinsheet', component: SigninsheetComponent },
  { path: 'entreprise', component: EntrepriseComponent }
];


@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }