import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { SigninsheetComponent } from './components/signinsheet/signinsheet.component';



@NgModule({
  declarations: [SigninsheetComponent],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class SigninsheetModule { }
