import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigninsheetComponent } from './signinsheet.component';

describe('SigninsheetComponent', () => {
  let component: SigninsheetComponent;
  let fixture: ComponentFixture<SigninsheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigninsheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninsheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
