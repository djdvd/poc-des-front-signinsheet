import { Component, OnInit } from '@angular/core';

import { Signinsheet } from '../../../../core/models/signinsheet/signinsheet.model';
import { Student } from '../../../../core/models/student/student.model';
import { Training } from '../../../../core/models/training/training.model';
import { StudentService } from '../../../../core/services/student/student.service';

@Component({
  selector: 'app-signinsheet',
  templateUrl: './signinsheet.component.html',
  styleUrls: ['./signinsheet.component.css']
})
export class SigninsheetComponent implements OnInit {

  id: number;
  student: Student = new Student();
  training: Training = new Training();
  signinsheet: Signinsheet = new Signinsheet();
  submitted = false;

  constructor(
    private studentService: StudentService
    ) { }

  ngOnInit(): void {
   // this.display();
  }

  save() {
    this.student.trainings.push(this.training);
    this.student.signInSheets.push(this.signinsheet);
    this.studentService
    .createStudent(this.student).subscribe(data => {
      console.log('test message data ****** : ' + data);
      this.student = new Student();
      
    },
    error => console.log(error));
  }

  newStudent(): void {
    this.submitted = false;
    this.student = new Student();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }


}
