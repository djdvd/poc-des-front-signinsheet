import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { StudentComponent } from './components/student/student.component';


@NgModule({
  declarations: [StudentComponent],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class StudentModule { }
