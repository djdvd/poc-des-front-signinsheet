import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Student } from '../../../../core/models/student/student.model';
import { StudentService } from '../../../../core/services/student/student.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  id: number;
  student: Student = new Student();
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private studentService: StudentService
    ) { }

  ngOnInit(): void {
   // this.display();
  }

  save() {
    this.studentService
    .createStudent(this.student).subscribe(data => {
      console.log('test message data : ' + data);
      this.student = new Student();
      this.gotoList();
    },
    error => console.log(error));
  }

  newStudent(): void {
    this.submitted = false;
    this.student = new Student();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  list () {
    this.router.navigate(['student']);
  }

  gotoList() {
    this.router.navigate(['/student']);
  }

  display() {
    this.studentService.getStudent(1).subscribe(data=>{
      this.student = data;
    });
  }

}
