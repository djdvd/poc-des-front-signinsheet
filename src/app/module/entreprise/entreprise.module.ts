import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntrepriseComponent } from './components/entreprise/entreprise.component';



@NgModule({
  declarations: [EntrepriseComponent],
  imports: [
    CommonModule
  ]
})
export class EntrepriseModule { }
