import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeacherComponent } from './components/teacher/teacher.component';



@NgModule({
  declarations: [TeacherComponent],
  imports: [
    CommonModule
  ]
})
export class TeacherModule { }
