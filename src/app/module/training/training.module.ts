import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainingComponent } from './components/training/training.component';



@NgModule({
  declarations: [TrainingComponent],
  imports: [
    CommonModule
  ]
})
export class TrainingModule { }
